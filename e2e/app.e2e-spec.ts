import { ResponsiveGridPage } from './app.po';

describe('responsive-grid App', () => {
  let page: ResponsiveGridPage;

  beforeEach(() => {
    page = new ResponsiveGridPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
