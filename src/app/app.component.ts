import { Component } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { DataService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  private activeSlide = 0;

  constructor(
    private _translateService: TranslateService
  ) {
    this._configureI18N();
  }

  private _configureI18N(): void {
    this._translateService.setDefaultLang('en');
    this._translateService.use('en');
  }

}
