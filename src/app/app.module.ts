import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AppRouting, AppRoutingProviders } from './app.routes';
import { EventModule } from './core/event';
import { GridModule } from './core/grid';
import { PagesModule } from './pages';
import { ServicesModule } from './services';

export function translateLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRouting,
    BrowserModule,
    EventModule,
    FormsModule,
    GridModule,
    HttpModule,
    PagesModule,
    ServicesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: ( translateLoaderFactory ),
        deps: [ Http ]
      }
    })
  ],
  providers: [ AppRoutingProviders ],
  bootstrap: [ AppComponent ],
  exports: [
    TranslateModule
  ]
})
export class AppModule { }
