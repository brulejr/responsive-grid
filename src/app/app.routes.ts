import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  AlbumsPageComponent,
  PhotosPageComponent,
  TodosPageComponent,
  UsersPageComponent
} from './pages';

const routes: Routes = [
  { path: 'albums', component: AlbumsPageComponent },
  { path: 'photos', component: PhotosPageComponent },
  { path: 'todos', component: TodosPageComponent },
  { path: 'users', component: UsersPageComponent }
];

export const AppRoutingProviders: any[] = [

];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(routes);
