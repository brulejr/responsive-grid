import { EventOptions } from './event-options';

export interface EventData {
  name: string;
  options?: EventOptions;
}
