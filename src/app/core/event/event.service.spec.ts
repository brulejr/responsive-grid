import { EventService } from './event.service';

describe('Service: EventService', () => {

  let service: EventService;

  beforeEach(() => {
    service = new EventService();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('#registerEventHandler should return a subscription', () => {
    let subscription = service.registerEventHandler('EVENT', (event) => console.log(event));
    expect(subscription).toBeTruthy();
    subscription.unsubscribe();
  });

  it('#postEvent should send a message to a matching event handler', done => {
    let count = 2;
    let subscription1 = service.registerEventHandler('TEST1', (event) => {
      expect(event).toBeDefined();
      expect(event.type).toBe('TEST1');
      expect(event.data).toBeDefined();
      expect(event.data.name).toBe('test.event.1');
      count -= 1;
    });
    let subscription2 = service.registerEventHandler('TEST2', (event) => {
      expect(event).toBeDefined();
      expect(event.type).toBe('TEST2');
      expect(event.data).toBeDefined();
      expect(event.data.name).toBe('test.event.2');
      count -= 1;
    });

    service.postEvent('TEST1', {name: 'test.event.1'});
    service.postEvent('TEST2', {name: 'test.event.2'});

    if (count === 0) {
      subscription1.unsubscribe();
      subscription2.unsubscribe();
      done();
    }
  });

  it('#postEvent should not fail when no handlers are defined', () => {
    service.postEvent('TEST_NO_HANDLERS', {name: 'test.event'});
  });

});
