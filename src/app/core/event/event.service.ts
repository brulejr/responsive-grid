import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

import { Event } from './event';
import { EventData } from './event-data';

@Injectable()
export class EventService {

  private _subject: Subject<Event>;

  constructor() {
    this._subject = new Subject<Event>();
  }

  postEvent(type: string, data: EventData) {
    this._subject.next({ type: type, data: data});
  }

  registerEventHandler(type: string, eventHandler): Subscription {
    return this._subject
      .filter(function(event) { return event.type === type; })
      .subscribe(eventHandler);
  }

}
