import { EventData } from './event-data';

export interface Event {
  type: string;
  data: EventData;
}
