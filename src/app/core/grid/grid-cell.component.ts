import { Component, Input, TemplateRef } from '@angular/core';

import * as _ from "lodash";

@Component({
  selector: 'grid-cell',
  templateUrl: './grid-cell.component.html',
  styleUrls: ['./grid-cell.component.less']
})
export class GridCellComponent {

  @Input() private name: string;
  @Input() private row: any;
  @Input() private template: TemplateRef<Object>

  private haveTemplate(): boolean {
    return !_.isUndefined(this.template);
  }

}
