import { Component, ContentChild, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'grid-column',
  templateUrl: './grid-column.component.html',
  styleUrls: ['./grid-column.component.less']
})
export class GridColumnComponent {

  @Input() name: string;
  @Input() label: string;
  @ContentChild('cellTemplate') cellTemplate: TemplateRef<Object>;
  @ContentChild('headerTemplate') headerTemplate: TemplateRef<Object>;

}
