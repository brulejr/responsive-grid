import { Component, Input, TemplateRef } from '@angular/core';

import * as _ from "lodash";

@Component({
  selector: 'grid-header',
  templateUrl: './grid-header.component.html',
  styleUrls: ['./grid-header.component.less']
})
export class GridHeaderComponent {

  @Input() private name: string;
  @Input() private template: TemplateRef<Object>

  private haveTemplate(): boolean {
    return !_.isUndefined(this.template);
  }

}
