import {
  AfterContentInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  TemplateRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { GridColumnComponent } from './grid-column.component';

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.less']
})
export class GridComponent implements AfterContentInit, OnInit, OnDestroy {

  @Input() private data: Observable<{ data: Array<any> }>;
  @Input() private tableClass: string = 'table table-striped table-hover';
  @ContentChildren(GridColumnComponent) columns: QueryList<GridColumnComponent>;

  private _dataCache: any[];
  private _dataSubscription: Subscription;

  constructor(private _changeDetectionRef : ChangeDetectorRef) {
  }

  findCellTemplate(name: string): TemplateRef<Object> {
    const matches: GridColumnComponent[] = this.columns.filter((c) => c.name === name);
    return matches.length > 0 ? matches[0].cellTemplate : undefined;
  }

  findHeaderTemplate(name: string): TemplateRef<Object> {
    const matches: GridColumnComponent[] = this.columns.filter((c) => c.name === name);
    return matches.length > 0 ? matches[0].headerTemplate : undefined;
  }

  ngAfterContentInit() {
  }
  
  ngOnDestroy() {
    if (this._dataSubscription) {
      this._dataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this._dataSubscription = this.data.subscribe(content => {
      this._dataCache = content.data;
      this._changeDetectionRef.detectChanges();
    });
  }

}
