import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GridComponent } from './grid.component';
import { GridCellComponent } from './grid-cell.component';
import { GridColumnComponent } from './grid-column.component';
import { GridHeaderComponent } from './grid-header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    GridComponent,
    GridCellComponent,
    GridColumnComponent,
    GridHeaderComponent
  ],
  exports: [
    GridComponent,
    GridColumnComponent
  ]
})
export class GridModule { }
