import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from '@ngx-translate/core';

import { Album } from '../model';
import { DataService } from '../services';
import { EventService } from '../core/event';

@Component({
  templateUrl: './albums-page.component.html',
  styleUrls: ['./albums-page.component.less']
})
export class AlbumsPageComponent implements OnInit {

  constructor(
    private _dataService : DataService,
    private _eventService: EventService,
    private _translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData(): void {
    this._eventService.postEvent('LOAD_DATA', { name: 'albums' });
  }

  loadData(): Observable<Album[]> {
    return this._dataService.albums$;
  }

}
