export * from './pages.module';

export * from './albums-page.component';
export * from './photos-page.component';
export * from './todos-page.component';
export * from './users-page.component';
