import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { AlbumsPageComponent } from './albums-page.component';
import { GridModule } from '../core/grid';
import { PhotosPageComponent } from './photos-page.component';
import { TodosPageComponent } from './todos-page.component';
import { UsersPageComponent } from './users-page.component';

@NgModule({
  imports: [
    GridModule,
    TranslateModule
  ],
  declarations: [
    AlbumsPageComponent,
    PhotosPageComponent,
    TodosPageComponent,
    UsersPageComponent
  ],
  exports: [
    AlbumsPageComponent,
    PhotosPageComponent,
    TodosPageComponent,
    UsersPageComponent
  ]
})
export class PagesModule {

}
