import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from '@ngx-translate/core';

import { Photo } from '../model';
import { DataService } from '../services';
import { EventService } from '../core/event';

@Component({
  templateUrl: './photos-page.component.html',
  styleUrls: ['./photos-page.component.less']
})
export class PhotosPageComponent implements OnInit {

  constructor(
    private _dataService : DataService,
    private _eventService: EventService,
    private _translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData(): void {
    this._eventService.postEvent('LOAD_DATA', { name: 'photos' });
  }

  loadData(): Observable<Photo[]> {
    return this._dataService.photos$;
  }

}
