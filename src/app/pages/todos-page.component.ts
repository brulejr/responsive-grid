import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from '@ngx-translate/core';

import { Todo } from '../model';
import { DataService } from '../services';
import { EventService } from '../core/event';

@Component({
  templateUrl: './todos-page.component.html',
  styleUrls: ['./todos-page.component.less']
})
export class TodosPageComponent implements OnInit {

  constructor(
    private _dataService : DataService,
    private _eventService: EventService,
    private _translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData(): void {
    this._eventService.postEvent('LOAD_DATA', { name: 'todos' });
  }

  loadData(): Observable<Todo[]> {
    return this._dataService.todos$;
  }

}
