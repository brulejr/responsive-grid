import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from '@ngx-translate/core';

import { User } from '../model';
import { DataService } from '../services';
import { EventService } from '../core/event';

@Component({
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.less']
})
export class UsersPageComponent implements OnInit {

  constructor(
    private _dataService : DataService,
    private _eventService: EventService,
    private _translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData(): void {
    this._eventService.postEvent('LOAD_DATA', { name: 'users' });
  }

  loadData(): Observable<User[]> {
    return this._dataService.users$;
  }

}
