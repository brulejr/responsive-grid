import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import * as _ from 'lodash';

import { EventService, EventOptions } from '../core/event';

export class DataStore<T> {

  private _data: BehaviorSubject<Object> = new BehaviorSubject([]);
  private _dataLoader: Subscription;
  private _eventOptions: EventOptions = [];
  private _timestamp: number = _.now();

  constructor(
     private _name: string,
     private _uri: string,
     private _http: Http,
     private _eventService: EventService,
     private _cacheTimeout: number = 60 * 1000
   ) {
     this._dataLoader = _eventService.registerEventHandler('LOAD_DATA', (event) => {
       if ((event.data.name === this._name) || (event.data.name === 'GLOBAL')) {
         const eventOptions: EventOptions = event.data.options;
         if (this._isExpired(eventOptions)) {
           this._load(eventOptions);
           this._eventOptions = eventOptions;
           this._timestamp = _.now();
         }
       }
     });
   }

   destroy(): void {
     this._dataLoader.unsubscribe();
   }

   get data(): Observable<T[]> {
     return new Observable<T[]>(fn => this._data.subscribe(fn));
   }

   private _isExpired(eventOptions: EventOptions) {
     const different = (eventOptions !== this._eventOptions);
     const old = _.now() > this._timestamp + this._cacheTimeout;
     const expired = different ? true : old;
     return expired;
   }

   private _load(eventOptions: EventOptions) {
     this._http
       .get(this._uri)
       .map(response => response.json())
       .subscribe(data => {
         this._data.next({ data: data });
       }, error => {
         const errmsg = 'Could not load data from "' + this._uri + '"!';
         console.error(errmsg, error);
         this._eventService.postEvent('ERROR', {name: this._name, options: {
           error: errmsg,
           uri: this._uri,
           status: error.status
         }});
       });
   }

}
