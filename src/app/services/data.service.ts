import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';

import { DataStore } from './data-store.service';
import { EventService, EventOptions } from '../core/event';
import { Album } from '../model';
import { Photo } from '../model';
import { Todo } from '../model';
import { User } from '../model';

@Injectable()
export class DataService {

  albums$: Observable<Album[]>;
  photos$: Observable<Photo[]>;
  todos$: Observable<Todo[]>;
  users$: Observable<User[]>;

  private _albums: DataStore<Album>;
  private _photos: DataStore<Photo>;
  private _todos: DataStore<Todo>;
  private _users: DataStore<User>;

  constructor(
    private _http: Http,
    private _eventService: EventService
  ) {
    this._albums = this._loadFromStore('albums', 'https://jsonplaceholder.typicode.com/albums');
    this._photos = this._loadFromStore('photos', 'https://jsonplaceholder.typicode.com/photos');
    this._todos = this._loadFromStore('todos', 'https://jsonplaceholder.typicode.com/todos');
    this._users = this._loadFromStore('users', 'https://jsonplaceholder.typicode.com/users');

    this.albums$ = this._albums.data;
    this.photos$ = this._photos.data;
    this.todos$ = this._todos.data;
    this.users$ = this._users.data;
  }

  private _loadFromStore<T>(type: string, uri: string): DataStore<T> {
    return new DataStore<T>(type, uri, this._http, this._eventService);
  }

}
